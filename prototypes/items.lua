data:extend(
{
	{
		type = "item",
		name = "u-pipe-short",
		icon = "__ext_undergroundpipes__/graphics/icon/u-pipe-short.png",
		icon_size = 32,
		flags = {"goes-to-quickbar"},
		subgroup = "energy-pipe-distribution",
		order = "a[pipe]-b[pipe-to-ground]",
		place_result = "u-pipe-short",
		stack_size = 50
	},
	{
		type = "item",
		name = "u-pipe-long",
		icon = "__ext_undergroundpipes__/graphics/icon/u-pipe-long.png",
		icon_size = 32,
		flags = {"goes-to-quickbar"},
		subgroup = "energy-pipe-distribution",
		order = "a[pipe]-b[pipe-to-ground]",
		place_result = "u-pipe-long",
		stack_size = 50
	}
})