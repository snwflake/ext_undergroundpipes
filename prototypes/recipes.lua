data:extend(
{
  {
    type = "recipe",
    name = "u-pipe-short",
    ingredients =
    {
      {"pipe-to-ground", 2},
      {"iron-plate", 5}
    },
    result_count = 2,
    result = "u-pipe-short",
    requester_paste_multiplier = 4
  },
  {
    type = "recipe",
    name = "u-pipe-long",
    ingredients =
    {
      {"pipe-to-ground", 10},
      {"iron-plate", 25}
    },
    result_count = 2,
    result = "u-pipe-long",
    requester_paste_multiplier = 4
  }
})