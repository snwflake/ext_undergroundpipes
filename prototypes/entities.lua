function custom_pipe_covers(type)
  return
  {
    north =
    {
      layers =
      {
        {
          filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-cover-north-" .. type .. ".png",
          priority = "high",
          width = 64,
          height = 64,
          hr_version =
          {
            filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-cover-north-" .. type .. ".png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5
          }
        },
        {
          filename = "__base__/graphics/entity/pipe-covers/pipe-cover-north-shadow.png",
          priority = "high",
          width = 64,
          height = 64,
          draw_as_shadow = true,
          hr_version =
          {
            filename = "__base__/graphics/entity/pipe-covers/hr-pipe-cover-north-shadow.png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5,
            draw_as_shadow = true
          }
        },
      },
    },
    east =
    {
      layers =
      {
        {
          filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-cover-east-" .. type .. ".png",
          priority = "high",
          width = 64,
          height = 64,
          hr_version =
          {
            filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-cover-east-" .. type .. ".png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5
          }
        },
        {
          filename = "__base__/graphics/entity/pipe-covers/pipe-cover-east-shadow.png",
          priority = "high",
          width = 64,
          height = 64,
          draw_as_shadow = true,
          hr_version =
          {
            filename = "__base__/graphics/entity/pipe-covers/hr-pipe-cover-east-shadow.png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5,
            draw_as_shadow = true
          }
        },
      },
    },
    south =
    {
      layers =
      {
        {
          filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-cover-south-" .. type .. ".png",
          priority = "high",
          width = 64,
          height = 64,
          hr_version =
          {
            filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-cover-south-" .. type .. ".png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5
          }
        },
        {
          filename = "__base__/graphics/entity/pipe-covers/pipe-cover-south-shadow.png",
          priority = "high",
          width = 64,
          height = 64,
          draw_as_shadow = true,
          hr_version =
          {
            filename = "__base__/graphics/entity/pipe-covers/hr-pipe-cover-south-shadow.png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5,
            draw_as_shadow = true
          }
        },
      },
    },
    west =
    {
      layers =
      {
        {
          filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-cover-west-" .. type .. ".png",
          priority = "high",
          width = 64,
          height = 64,
          hr_version =
          {
            filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-cover-west-" .. type .. ".png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5
          }
        },
        {
          filename = "__base__/graphics/entity/pipe-covers/pipe-cover-west-shadow.png",
          priority = "high",
          width = 64,
          height = 64,
          draw_as_shadow = true,
          hr_version =
          {
            filename = "__base__/graphics/entity/pipe-covers/hr-pipe-cover-west-shadow.png",
            priority = "extra-high",
            width = 128,
            height = 128,
            scale = 0.5,
            draw_as_shadow = true
          }
        },
      },
    }
  }
end

function custom_pipe_pictures(type)
  return
  {
    up =
      {
        filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-up-" .. type .. ".png",
        priority = "high",
        width = 64,
        height = 64,
        hr_version =
        {
          filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-up-" .. type .. ".png",
          priority = "extra-high",
          width = 128,
          height = 128,
          scale = 0.5
        }
      },
      down =
      {
        filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-down-" .. type .. ".png",
        priority = "high",
        width = 64,
        height = 64,
        hr_version =
        {
          filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-down-" .. type .. ".png",
          priority = "extra-high",
          width = 128,
          height = 128,
          scale = 0.5
        }
      },
      left =
      {
        filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-left-" .. type .. ".png",
        priority = "high",
        width = 64,
        height = 64,
        hr_version =
        {
          filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-left-" .. type .. ".png",
          priority = "extra-high",
          width = 128,
          height = 128,
          scale = 0.5
        }
      },
      right =
      {
        filename = "__ext_undergroundpipes__/graphics/entity/u-pipe-right-" .. type .. ".png",
        priority = "high",
        width = 64,
        height = 64,
        hr_version =
        {
          filename = "__ext_undergroundpipes__/graphics/entity/hr-u-pipe-right-" .. type .. ".png",
          priority = "extra-high",
          width = 128,
          height = 128,
          scale = 0.5
        }
      }
  }
end

data:extend(
{
  {
    type = "pipe-to-ground",
    name = "u-pipe-short",
    icon = "__ext_undergroundpipes__/graphics/icon/u-pipe-short.png",
	  icon_size = 32,
    flags = {"placeable-neutral", "player-creation"},
    minable = {hardness = 0.2, mining_time = 0.5, result = "u-pipe-short"},
    max_health = 50,
    corpse = "small-remnants",
    resistances =
    {
      {
        type = "fire",
        percent = 80
      }
    },
    collision_box = {{-0.29, -0.29}, {0.29, 0.2}},
    selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
    fluid_box =
    {
      base_area = 1,
      pipe_covers = custom_pipe_covers("short"),
      pipe_connections =
      {
        { position = {0, -1} },
        {
          position = {0, 1},
          max_underground_distance = 50
        }
      },
    },
    underground_sprite =
    {
      filename = "__core__/graphics/arrows/underground-lines.png",
      priority = "extra-high-no-scale",
      width = 64,
      height = 64,
      scale = 0.5
    },
    vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
    pictures = custom_pipe_pictures("short")
  },
  {
    type = "pipe-to-ground",
    name = "u-pipe-long",
    icon = "__ext_undergroundpipes__/graphics/icon/u-pipe-long.png",
	  icon_size = 32,
    flags = {"placeable-neutral", "player-creation"},
    minable = {hardness = 0.2, mining_time = 0.5, result = "u-pipe-long"},
    max_health = 50,
    corpse = "small-remnants",
    resistances =
    {
      {
        type = "fire",
        percent = 80
      }
    },
    collision_box = {{-0.29, -0.29}, {0.29, 0.2}},
    selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
    fluid_box =
    {
      base_area = 1,
      pipe_covers = custom_pipe_covers("long"),
      pipe_connections =
      {
        { position = {0, -1} },
        {
          position = {0, 1},
          max_underground_distance = 250
        }
      },
    },
    underground_sprite =
    {
      filename = "__core__/graphics/arrows/underground-lines.png",
      priority = "extra-high-no-scale",
      width = 64,
      height = 64,
      scale = 0.5
    },
    vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
    pictures = custom_pipe_pictures("long")
  }
})