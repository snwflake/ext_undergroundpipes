VERSION=$(cat info.json | gawk '/"version":/' | grep -Eo "([0-9]+\.[0-9]+\.[0-9]+)")
BASENAME="ext_undergroundpipes_"

rm -fv ${BASENAME}*.zip

rsync -av --exclude='*.git*' --exclude='build.sh' ./ ${BASENAME}${VERSION}
zip -r ${BASENAME}${VERSION}.zip ${BASENAME}${VERSION}

rm -rfv ${BASENAME}${VERSION}
