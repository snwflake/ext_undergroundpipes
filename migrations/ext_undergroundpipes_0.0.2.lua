-- File is only run once for every save game loaded
-- Naming pattern: ${mod_name}_${mod_version}.lua

-- Reload recipes and technologies
for i, player in ipairs(game.players) do
    player.force.reset_recipes()
    player.force.reset_technologies()
end